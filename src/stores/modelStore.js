import { defineStore } from 'pinia'
import { ref } from 'vue'

export const modelStore = defineStore('modelStore', () => {
  /**
   * Keeps track of which model was selected last
   */
  const currentModelId = ref(-1)

  /**
   * All model data
   */
  const availableModels = [
    {
      id: 0,
      poster: '/posters/posterProPakUNO.png',
      model: '/models/ProPakUNOV4_normalsAvaraged.glb',
      title: 'ProPak UNO',
      description: 'A 3D model of a breathing apparatus by asteam AG',
      camera: '90deg 90deg 5m',
      projectId: 0,
      featured: true
    },
    {
      id: 1,
      poster: '/posters/fireextinguisher.png',
      model: '/models/fireextinguisher.glb',
      title: 'Fire Extinguisher',
      description: 'Fire Extinguisher Powder Category ABC from Scania P124',
      camera: '0deg 90deg 2m',
      projectId: 1,
      featured: true
    },
    {
      id: 2,
      poster: '/posters/TP100.png',
      model: '/models/TP100.glb',
      title: 'Scania P124',
      description: 'Firetruck by Feumotech in Delémont in service since 2004',
      camera: '-90deg 90deg 10m',
      projectId: 1,
      featured: true
    }
  ]

  const projects = [
    {
      id: 0,
      title: 'asteam',
      path: 'asteam',
      client: 'asteam AG',
      url: 'https://asteam.ch',
      urlText: 'www.asteam.ch'
    },
    {
      id: 1,
      title: 'Firetruck VR',
      path: 'firetruck-vr',
      client: '', // Feumotech, not offiacl
      url: '',
      urlText: ''
    }
  ]

  return { currentModelId, availableModels, projects }
})
