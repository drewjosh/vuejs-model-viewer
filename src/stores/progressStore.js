import { defineStore } from 'pinia'

export const useProgressStore = defineStore('progressStore', () => {
  /**
   * All model data
   */
  const annotations = [
    {
      id: 0,
      title: 'Toolbox',
      dataPosition: '0.5532815595204621m -0.07238479478973979m 0.60751276892161m',
      dataNormal: '0.9979052960981577m -0.06396660216376425m 0.009658872960711187m',
      captured: true,
      sketchfab: false,
      unity: false,
      collider: false,
      grabPoint: false
    },
    {
      id: 1,
      title: 'Chainsaw Trousers',
      dataPosition: '0.5767330939143502m -0.07850727898073753m 0.2514976999673422m',
      dataNormal: '0.2883339977403456m 0.9469213660611343m -0.14213877811486117m',
      captured: true,
      sketchfab: true,
      unity: false,
      collider: false,
      grabPoint: false
    }
  ]

  return { annotations }
})
