# vuejs-model-viewer

Simple vuejs + vite app to display 3d models by using Googles model-viewer. 3d models created by [Firefighter Vision](https://firefightervision.com).

## Project

Prod-URL: [demo.firefightervision.com](https://demo.firefightervision.com)

Project start: 20.9.2023

Developer:

- [Joshua Drewlow](https://gitlab.com/drewjosh)

## License

- [MIT](https://gitlab.com/firefighter-vision/vuejs-model-viewer/-/blob/main/LICENSE)
- © All 3d models featured in this project belong to Firefighter Vision. All rights reserved.

## Technology stack

- [Vue 3](https://vuejs.org/)
- [Vite](https://vitejs.dev/)
- [model-viewer](https://modelviewer.dev/)
- [Three.js](https://threejs.org/)

## Changelog

| Version | Date       | Changes                                                                                                                                 |
| ------- | ---------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| 0.3.0   | 19.01.2024 | - added new route with truck progress wip                                                                                               |
| 0.2.1   | 21.09.2023 | - enable webxr support                                                                                                                  |
| 0.2.0   | 21.09.2023 | - added project filter<br />- removed additional loading button<br />- various UI improvements<br />- improvement component versatility |
| 0.1.0   | 20.09.2023 | - first version displaying three models                                                                                                 |
